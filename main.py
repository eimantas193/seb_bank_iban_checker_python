import sys
import csv
from tkinter import *

root = Tk()
root.title('IBAN checker')
root.geometry("635x220")
user_input = StringVar()
user_input_path = StringVar()
update_ans=StringVar()
update_path=StringVar()

def IBAN_enter_confirm(IBAN): # tikrinamas ivestas IBAN i laukeli
    bank,IBAN_status =valid_IBAN(IBAN.rstrip())
    if(bank!="SEB"): # modifikuojamos vertes, kad atitiktu salyga
        bank="Other"
    update_ans.set(IBAN_status+" "+bank) # isvedama informacija vortotojui

def IBAN_csv_confirm(path): # tikrinamas ivestas .txt failas
    new_name=[]
    accounts_numbers = open(path, "r")

    index=path.index('.') # nemetama failo galune tolimesniam modifikavimui
    for i in range(index):
        new_name.append(path[i])
    final_name = "".join(new_name)

    for string in accounts_numbers:
        IBAN=string.rstrip()
        bank,IBAN_status =valid_IBAN(IBAN)
        if(IBAN_status=="Valid"): # modifikuojamos vertes, kad atitiktu salyga
            IBAN_status="true"
        else:
            IBAN_status="false"
        if(bank=="Other"):
            bank=""

        with open(final_name+'_valid.csv', mode='a') as valid_accounts: # kuriamas <originalname>_valid.csv
            valid_accounts = csv.writer(valid_accounts, delimiter=';')
            valid_accounts.writerow([IBAN,IBAN_status])
        
        with open(final_name+'_bank.csv', mode='a') as bank_accounts: # kuriamas <originalname>_bank.csv
            bank_accounts = csv.writer(bank_accounts, delimiter=';')
            bank_accounts.writerow([IBAN,bank])
    update_path.set(final_name+'_valid.csv\n'+final_name+'_bank.csv') #isvedama keliai kur saugomi failai
    


def valid_IBAN(IBAN):
    error=False
    alph = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"] # raidziu sarasas paieskai
    letters=[]
    fix_value=[]
    bank_code_char=[]
    for i in range(2): # konvertuojamso raides i skaicius
        num = alph.index(IBAN[i])
        sk = int(num)+10
        try:
            letters.append(str(int(sk/10)))
            letters.append(str(int(sk%10)))
        except:
            error=True
            break
    if(error!=True):
        for i in range(len(IBAN)-4): #sutvarkomas IBAN kodas ir paruosiamas modulecijai
            if (IBAN[i+4]=='\n'):
                continue
            fix_value.append(IBAN[i+4])
        fix_value.append(letters[0])
        fix_value.append(letters[1])
        fix_value.append(letters[2])
        fix_value.append(letters[3])
        fix_value.append(IBAN[2])
        fix_value.append(IBAN[3])

        for i in range(5):
            bank_code_char.append(IBAN[i+4])
        try:
            bank_code = int("".join(bank_code_char)) # randami banku kodai
            if(bank_code==70440): # SEB bankas
                bank="SEB"
            elif(bank_code==71800): # Siauliu bankas
                bank="SB"
            elif(bank_code==73000): # Sweadbank bankas
                bank="SWB"
            else:
                bank="Other" # visi like bankai
        except:
            bank="Other"

        try:
            value = int("".join(fix_value)) # sujungiam vertes is saraso i skaiciuojama verte
            if(value%97==1): # jei po modulacijos gaunamas 1 reiskia saskaita gera
                status="Valid"
            else:
                status="Invalid"
        except:
            status="Invalid"
        return bank,status
    else:
        return "Other", "Invalid"
# inteface
label1=LabelFrame(root,padx=10,pady=5)
label1.grid(row=0, column=0, padx=5, pady=5)
Label(label1, text="IBAN number checher").pack()

label2=LabelFrame(root,padx=10,pady=5)
label2.grid(row=1, column=0, padx=5, pady=5)
Label(label2, text="Answerer:").pack()

label3=LabelFrame(root,padx=10,pady=5)
label3.grid(row=2, column=0, padx=5, pady=5)

label4=LabelFrame(root,padx=10,pady=5)
label4.grid(row=0, column=1, padx=10, pady=5)
Label(label4, text="IBAN.txt file path:").pack()

label5=LabelFrame(root,padx=10,pady=5)
label5.grid(row=1, column=1, padx=10, pady=5)
Label(label5, text="CSV file save location:").pack()

label6=LabelFrame(root,padx=10,pady=5)
label6.grid(row=2, column=1, padx=5, pady=5)

IBAN_enter = Entry(label1,textvariable = user_input,width=22).pack()
IBAN_exit = Label(label2, textvariable = update_ans).pack()
Button(label3, text="OK", command=lambda: IBAN_enter_confirm(user_input.get())).pack()
IBAN_csv_path = Entry(label4,textvariable = user_input_path,width=40).pack()
IBAN_exit = Label(label5, textvariable = update_path).pack()
Button(label6, text="OK", command=lambda: IBAN_csv_confirm(user_input_path.get())).pack()


root.mainloop()