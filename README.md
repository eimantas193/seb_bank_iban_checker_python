Jei naudojamasi Linux per terminalą leidžiama "python3 IBAN_checker.pyc"<br/>
Jei naudojama Windows einama į "Windows_launcher" aplankalą ir iš ten leidžiama "IBAN_checker.exe"<br/>
Jei norima paleisti orginalų failą per terminalą leidžiama "python3 main.py". Reikalinga "Tkinter" paketas.

Programos viduje yra dvi skiltys kairėje vedamas IBAN dešinėje .txt failas su banko IBAN sąskaitom kelais.<br/>
Paspaudus "OK" kairėje gaunama ar sąskaita validi ar ne ir ar ji priklauso SEB.<br/>
Dešinėje paspaudus "OK" gaunamas kelias kur išsaugomi failai (pagal nutylėjimą ten kur yra orginalus txt failas)<br/>

